# Générateur de flux RSS non tronqué d’Arrêt sur images

Le flux RSS d’Arrêt sur images est modifié pour contenir les articles en entier et non de manière tronquée.

La modification du flux nécessite un compte et un [abonnement valide](https://www.arretsurimages.net/?formula=all).

## Dépendances

```
apt install libmojolicious-perl libxml-rss-perl
```

## Configuration

La configuration est gérée par trois variables d’environnement :
- `ASI_RSSFILE` : chemin du fichier RSS à créer
- `ASI_LOGIN` : l’email utilisé pour votre compte sur Arrêt sur images
- `ASI_PWD` : le mot de passe de votre compte sur Arrêt sur images

## Installation

```
wget https://framagit.org/fiat-tux/rss/asi-rss/-/raw/main/asi-rss.pl -O /opt/asi-rss.pl
chmod +x /opt/asi-rss.pl
```

## Utilisation

```
ASI_RSSFILE="/var/www/mon_flux_rss_des_actus_asi.rss" ASI_LOGIN="me@exemple.org" ASI_PWD="foobarbaz" /opt/asi-rss.pl
```

## Cron

Voici un exemple de tâche cron pour mettre à jour le flux toutes les 6 heures :
```
45 */6 * * * ASI_RSSFILE="/var/www/mon_flux_rss_des_actus_asi.rss" ASI_LOGIN="me@exemple.org" ASI_PWD="foobarbaz" /opt/asi-rss.pl
```

## Licence

Affero GPLv3. Voir le fichier [LICENSE](LICENSE).
