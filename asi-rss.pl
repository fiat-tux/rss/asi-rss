#!/usr/bin/env perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use Mojo::Base -base;
use Mojo::DOM;
use Mojo::JSON qw(encode_json decode_json);
use Mojo::URL;
use Mojo::UserAgent;
use Mojo::Util qw(decode);
use Mojo::Collection 'c';
use XML::RSS;

my $login = decode('UTF-8', $ENV{ASI_LOGIN});
my $pwd   = decode('UTF-8', $ENV{ASI_PWD});
my $file  = $ENV{ASI_RSSFILE};

die 'Please set ASI_LOGIN, ASI_PWD and ASI_RSSFILE. Aborting.' unless ($login && $pwd && $file);

my $ua   = Mojo::UserAgent->new();
   $ua->max_redirects(3);
my $feed = XML::RSS->new();

# Connection
my $args = {
    client_id     => '1_1e3dazertyukilygfos7ldzertyuof7pfd',
    client_secret => '2r8yd4a8un0fn45d93acfr3efrgthzdheifhrehihidg4dk5kds7ds23',
    username      => $login,
    password      => $pwd,
    grant_type    => 'password'
};
my $token = $ua->post("https://api.arretsurimages.net/oauth/v2/token" => json => $args)->result->json->{access_token};

# Get articles
$feed->parse($ua->get('https://api.arretsurimages.net/api/public/rss/all-content')->result->body);
foreach my $item (@{$feed->{'items'}}) {
    my $link = Mojo::URL->new($item->{link});
    if (index($link->path, '/articles/') != -1) {
        $item->{title} = 'Article — '.$item->{title};
    } elsif (index($link->path, '/chroniques/') != -1) {
        $item->{title} = 'Chronique — '.$item->{title};
    } elsif (index($link->path, '/emissions/') != -1) {
        $item->{title} = 'Émission — '.$item->{title};
    }
    $link->host('api.arretsurimages.net')
         ->path('/api/public/contents'.$link->path)
         ->query(access_token => $token);

    my $json        = decode_json($ua->get($link)->result->body);
    my $description = $json->{content};
    my $dom         = Mojo::DOM->new($description);
    my $asi_html    = $dom->find('asi-html');
    $dom->find('q')->each(sub {
        my ($e, $num) = @_;
        $e->replace($e->content);
    });
    $dom->find('asi-image')->each(sub {
        my ($e, $num) = @_;
        $e->replace($e->content);
    });
    $dom->find('asi-video')->each(sub {
        my ($e, $num) = @_;
        $e->replace($e->content);
    });
    if ($asi_html->size) {
        $asi_html->each(sub {
            my ($e, $num) = @_;
            $e->replace($e->attr('data-html')) if $e->attr('data-html');
        });
    }
    $description = $dom->to_string;
    $description = sprintf("<p><strong>%s</strong></p>\n%s", $json->{lead}, $description) if $json->{content};
    if ($json->{cover} && $json->{cover}->{formats} && $json->{cover}->{formats}->{article_header}) {
        $description = sprintf("<img src=\"https://api.arretsurimages.net/api/public/media/%s/action/show?format=article_header\" alt=\"\">\n%s",
            $json->{cover}->{slug}, $description);
    }
    $description =~ s#“#« #g;
    $description =~ s#”# »#g;
    if (defined($json->{associated_video})) {
        my $embed;
        if ($json->{associated_video}->{reference_url} =~ m#https://www\.youtube\.com/watch\?v=#) {
            $embed = $json->{associated_video}->{reference_url};
            $embed =~ s#https://www\.youtube\.com/watch\?v=(.*)#https://www.youtube-nocookie.com/embed/$1#;
        }
        if (ref($json->{associated_video}->{metas}) eq 'HASH' && defined($json->{associated_video}->{metas}->{embed_url})) {
            $embed = $json->{associated_video}->{metas}->{embed_url};
        }
        $description .= sprintf("\n".'<iframe src="%s" allow="autoplay; fullscreen" allowfullscreen="" title="2020-04-15-JLou-Metaphores" data-ready="true" width="967" height="544" frameborder="0"></iframe>', $embed) if $embed;
    };

    $item->{description} = $description if $description;

    $item->{author} = c(@{$json->{'authors'}})->map(sub { $_->{'name'} })->join(', ');
}
$feed->save($file);
